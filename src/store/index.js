/**
 *  @desc Stores application state
 *  @param Vuex
 *  @retrurn navigation paths for components
 */

import Vue from 'vue'
import Vuex from 'vuex'



// Installing Vuex via Vue.use()
Vue.use(Vuex)
// Stoes path route for components
export default new Vuex.Store({
  state: {
    routeName: { bachelorPath: 'Bachelor-show', internshipPath: 'Internship-show', suggBachPath:'suggBachShow', suggIntPath: 'suggIntShow', bachAppPath: 'BachelorApplicationShow', intAppPath: 'InternshipApplicationShow', archivePath: 'ArchiveShow' }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
