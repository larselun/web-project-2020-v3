import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import firebase from 'firebase'
import './components/firebaseInit'

Vue.config.productionTip = false

let app
firebase.auth().onAuthStateChanged(user => {
  if(!app) {
    app = new Vue({
      store,
      router,
      render: h => h(App)
    }).$mount('#app')
  }
})


