/**
 *  @desc Using Vue Router to map components to path routes and where to render them
 *  @param Vue router from vue library and view components for src/view
 *  @retrurn Vue router object containing mapping and routes
 */

// Importing components
import Vue from "vue";
import VueRouter from "vue-router";
import firebase from "firebase";
import Home from "../views/Home.vue";
import LoginShow from "../views/LoginShow.vue";
import StudBachList from "../views/StudBachList.vue";
import BachelorShow from "../views/BachelorShow.vue";
import StudIntList from "../views/StudIntList.vue";
import InternshipShow from "../views/InternshipShow.vue";
import Inbox from "../views/Inbox.vue";
import SignupShow from "../views/SignupShow.vue";
import RegisterForm from "../views/RegisterForm.vue";
import bachelorContract from "../components/bachelorContract.vue";
import internshipContract from "../components/internshipContract.vue";
import Admin from "../views/Admin.vue";
import User from "../views/User.vue";
import Company from "../views/Company.vue";
import NoAccess from "../views/NoAccess.vue";
import CompanyList from "../views/CompanyList.vue";
import suggestedBachelorThesis from "../views/suggestedBachelorThesis.vue";
import suggBachShow from "../views/suggBachShow.vue";
import suggestedInternships from '../views/suggestedInternships.vue';
import suggIntShow from '../views/suggIntShow.vue';
import BachelorApplication from '../views/bachelorApplication';
import InternshipApplication from '../views/internshipApplication';
import BachelorApplicationShow from '../views/bachelorApplicationShow';
import InternshipApplicationShow from '../views/internshipApplicationShow';
import ArchiveList from '../views/ArchiveList';
import ArchiveShow from '../views/ArchiveShow';
import activeApplications from '../views/activeApplications.vue'

// Installing the router via Vue.use()
Vue.use(VueRouter);

// Adding path, name, component name and authentication guards for all views and components
const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
    },
    {
      path: "/login",
      name: "Login",
      component: LoginShow,
    },
    {
      path: "/signup",
      name: "Signup",
      component: SignupShow,
    },
    {
      path: "/student-bachelor-list",
      name: "StudBachList",
      component: StudBachList,
      meta: {
        requiresAuth: true,
        companyAuth: false,
      },
    },
    {
      path: "/student-bachelor-list/:id",
      name: "Bachelor-show",
      component: BachelorShow,
      props: true,
      meta: {
        requiresAuth: true, companyAuth: false, 
      },
    },
    {
      path: "/student-internship-list",
      name: "StudIntList",
      component: StudIntList,
      meta: {
        requiresAuth: true,
        companyAuth: false,
      },
    },
    {
      path: "/student-internship-list/:id",
      name: "Internship-show",
      component: InternshipShow,
      props: true,
      meta: {
        requiresAuth: true, companyAuth: false
      },
    },
    {
      path: "/inbox",
      name: "Inbox",
      component: Inbox,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/register-form",
      name: "RegisterForm",
      component: RegisterForm,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/bachelor-contract",
      name: "bachelorContract",
      component: bachelorContract,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/internship-contract",
      name: "internshipContract",
      component: internshipContract,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/admin-page",
      name: "adminPage",
      component: Admin,
      meta: {
        requiresAuth: true, adminAuth: true
      },
    },
    {
      path: "/user-page",
      name: "userPage",
      component: User,
      meta: {
        requiresAuth: true, studAuth: true
      },
    },
    {
      path: "/company-page",
      name: "companyPage",
      component: Company,
      meta: {
        requiresAuth: true, companyAuth: true
      },
    },
    {
      path: "/access-denied",
      name: "NoAccess",
      component: NoAccess,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/companies-list",
      name: "CompanyList",
      component: CompanyList,
      meta: {
        requiresAuth: true
      },
    },
    {
      path: "/suggested-bachelor-thesis",
      name: "SuggestedBachelorThesis",
      component: suggestedBachelorThesis,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path: "/suggested-bachelor-thesis/:id",
      name: "suggBachShow",
      component: suggBachShow,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path: "/suggested-internships",
      name: "suggestedInternships",
      component: suggestedInternships,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path:"/suggested-internship/:id",
      name: "suggIntShow",
      component: suggIntShow,
      meta: {
        requiresAuth: true, adminAuth: true,
      }
    },
    {
      path:"/bachelor-application",
      name: "BachelorApplication",
      component: BachelorApplication,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path:"/bachelor-application/:id",
      name: "BachelorApplicationShow",
      component: BachelorApplicationShow,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path:"/internship-application",
      name: "InternshipApplication",
      component: InternshipApplication,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path:"/internship-application/:id",
      name: "InternshipApplicationShow",
      component: InternshipApplicationShow,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path: "/archive",
      name: "ArchiveList",
      component: ArchiveList,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path: "archive/:id",
      name: "ArchiveShow",
      component: ArchiveShow,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
    {
      path: "active-applications",
      name: "ActiveApplications",
      component: activeApplications,
      meta: {
        requiresAuth: true, adminAuth: true
      }
    },
  ],
});

// Global navigation guards called whenever navigation is triggerd. Allowes for nagivigation to be guarded depending on user roles
router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  const adminAuth = to.matched.some((record) => record.meta.adminAuth);
  const studAuth = to.matched.some((record) => record.meta.studAuth);
  const companyAuth = to.matched.some((record) => record.meta.companyAuth);

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      firebase
        .auth()
        .currentUser.getIdTokenResult()
        .then(function({ claims }) {
          
          if (requiresAuth && currentUser) {
            next();
          }

          if (studAuth && !claims.student) {
            next("/access-denied");
          } else if (studAuth && claims.student) {
            next();
          }

          if (adminAuth && !claims.admin) {
            next("/access-denied");
          } else if (adminAuth && claims.admin) {
            next();
          }

          if (companyAuth && !claims.company) {
            next("/access-denied");
          } else if (companyAuth && claims.company) {
            next();
          }
        });

    } else if (requiresAuth && !currentUser) {
      next("/login");
    } else {
      next();
    } 
  });
});

export default router;
