
const functions = require('firebase-functions')
const admin = require('firebase-admin')

admin.initializeApp()

const db = admin.firestore()

// On signup
exports.AddUserRole = functions.auth.user().onCreate(async (authUser) => {
    // Checks is user meets role criteria
    if (authUser.email) {
        const customClaims = {
            // All new users is given role as user
            student: true
        }
            try { // Setting custom user claims to new user
            await admin.auth().setCustomUserClaims(authUser.uid, customClaims)
            const rolesRef = db.collection("users").doc(authUser.uid)
            return rolesRef.set({
                email: authUser.email,
                role: customClaims
            }, { merge: true });
        }
        catch (error) {
            console.log(error)
        }
    }  
})

exports.setUserRole = functions.https.onCall(async (data, context) => {
    if (!context.auth.token.admin) return

    try {
        await admin.auth().setCustomUserClaims(data.uid, data.role)
        const userRole = db.collection("users").doc(data.uid)
        return userRole.update({
            role: data.role
        })
    } catch (error) {
        console.log(error)
    }
})